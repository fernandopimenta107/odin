package com.maxim.jms.producer.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.maxim.jms.producer.model.Vendor;
import com.maxim.jms.producer.service.MessageService;

@Controller
public class ProducerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProducerController.class);

	@Autowired
	private MessageService messageSrvice;
	
	@RequestMapping("/")
	public String renderRevendorPage(Vendor vendor, Model model) {

		LOGGER.info("Rendering index JSP");
		return "index";
	}

	@RequestMapping(value="/vendor", method=RequestMethod.POST)
	public ModelAndView processRequest(@ModelAttribute("vendor") Vendor vendor, Model model) {

		LOGGER.info("Processing vendor object");
		LOGGER.info(vendor.toString());
		
		messageSrvice.process(vendor);

		ModelAndView mv = new ModelAndView();
		mv.setViewName("index");
		mv.addObject("message","Vendor Added Successfully");
		
		vendor = new Vendor();
		
		mv.addObject("vendor", vendor);

		return mv;

	}
}
