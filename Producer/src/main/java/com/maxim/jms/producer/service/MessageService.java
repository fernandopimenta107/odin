package com.maxim.jms.producer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.maxim.jms.producer.model.Vendor;
import com.maxim.jms.producer.sender.MessageSender;

@Service
public class MessageService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageService.class);

	@Autowired
	private MessageSender sender;
	
	public void process(Vendor vendor) {
		
		Gson gson = new Gson();
		String json = gson.toJson(gson);
		LOGGER.info("Message: "+json);
		sender.send(json);
	}

	
}
