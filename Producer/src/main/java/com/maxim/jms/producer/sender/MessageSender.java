package com.maxim.jms.producer.sender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class MessageSender {

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageSender.class);

	@Autowired
	private JmsTemplate jmsTemplate;

	public void send(String json) {
			
		try {
			jmsTemplate.convertAndSend(json);
			LOGGER.info("Message sent!");
		} catch (JmsException e) {
			LOGGER.error("Message:" +json,e);
		}
	}

}
