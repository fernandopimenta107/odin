<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
	<head>
		<meta charset="utf-8">
		<title>Vendor Entry</title>
		<link rel="stylesheet" href="<c:url value="/css/style.css" />" />
	</head> 
	<body>
	    <div>
	    
	    	<img alt="Vendor Entry" src="<c:url value="/images/img2.png" />">
	    </div>
		<div><h2>Vendor Entry</h2></div>
		<div id="form">
		  <form:form modelAttribute="vendor" action="vendor" cssClass="formulario">
		  	<div class="message">
		  		<c:if test="${!empty message }">
		  		  <c:out value="${message}"></c:out>
		  		</c:if>
		  	</div>
		  	<fieldset>
		  	   <legend>Vendor Information</legend>
		  	   	<div>
			  		<label for="vendorName">Vendor Name</label>
			  		<form:input path="vendorName" cssClass="txt"/>   
		  	   	</div>
		  	   	
		  	   	<div>
			  		<label for="firstName">First Name</label>
			  		<form:input path="firstName"/>   
		  	   	</div>
		  	   	
		  	   	<div>
			  		<label for="lastName">Last Name</label>
			  		<form:input path="lastName" cssClass="txt"/>   
		  	   	</div>
		  	   	
		  	   	<div>
			  		<label for="address">address</label>
			  		<form:input path="address"/>   
		  	   	</div>
		  	   	
		  	   	<div>
			  		<label for="city">city</label>
			  		<form:input path="city" cssClass="txt"/>   
		  	   	</div>
		  	   	
		  	   	<div>
			  		<label for="state">state</label>
			  		<form:input path="state" cssClass="txt"/>   
		  	   	</div>
		  	   	
		  	   	<div>
			  		<label for="zipCode">zipCode</label>
			  		<form:input path="zipCode" cssClass="txt"/>   
		  	   	</div>
		  	   	
		  	   	<div>
			  		<label for="phoneNumber">phoneNumber</label>
			  		<form:input path="phoneNumber" cssClass="txt"/>   
		  	   	</div>
		  	   	
		  	   	<div>
			  		<label for="email">email</label>
			  		<form:input path="email" cssClass="txt"/>   
		  	   	</div>
		  	   	
		  	   	<div>
		  	   		<input class="postar" type="submit" value="Submit">
		  	   	</div>
		  	
		  	
		  	
		  	</fieldset>
		  
		  </form:form>
		</div>
	</body>
</html>
